# scd

## CronExpr

> * 目前该框架的Cron只支持start-end/step,start-end/step格式，必须全部是数字，不支持英文缩写，特殊字符只支持一个L（表示最后一天）
> * start-end/step是通式，即可以只有列表(1,3,5)、有步长(0/2)、有区间(5-20)、步长加列表(0/2,0/4)、区间步长加列表(10-30/2,30-50/4)
> * 框架内置，秒，分，时，日级别的跳跃优化，性能可以使用下面的代码进行测试，内置的智能跳跃算法无论大小跨度都能保证稳定的性能
> * 如存在bug或者建议，请提交issue

* 测试案列，性能达ns级别
``` cangjie
//打印最近十次的执行时间点
//大跨度：0 0 0 L 2 ?
let cron = CronExpr.fromStr('0/5 * * * * ?')
let start = DateTime.now()
var current = DateTime.now()
var result = ArrayList<DateTime>()
for (_ in 1..11) {
    current = cron.getTimeAfter(current)
    result.add(current)
}
let times = (DateTime.now() - start)
for (pattern in result) {
    println(pattern.format("yyyy-MM-dd HH:mm:ss"))
}
println(times)
```

## QuickStart

scd后续会集成到我的[asp](https://gitcode.com/soulsoft/asp)框架，享受到依赖注入等功能

``` cangjie
let trigger = TriggerBuilder.createCron('0/5 * * * * ?').startNow().build()
let jobDetail = JobDetailBuilder.create<TestJob>().build()
let factory = SchedulerFactory.crea

te()
let scheduler = factory.getScheduler()
scheduler.scheduleJob(jobDetail, trigger)
scheduler.start()
println("started")
Console.stdIn.readln()
//job
public class TestJob <: IJob {
    public func execute(_: JobExecutionContext) {
        println("${DateTime.now().format("yyyy-MM-dd HH:mm:ss")}:woring...")
    }
}
```




