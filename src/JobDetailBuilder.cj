package scd

import std.time.*
import std.reflect.*
import scd.utils.*

public class JobDetailBuilder {
    private var _name: String
    private var _group: String
    private var _typeInfo: TypeInfo
    private var _descripton: ?String = None
    private var _isAllowConcurrentExecution = false

    private init(name: String, group: String, typeInfo: TypeInfo) {
        _name = name
        _group = group
        _typeInfo = typeInfo
    }

    public static func create(typeInfo: TypeInfo) {
        if (!typeInfo.isSubtypeOf(TypeInfo.of<IJob>())) {
            throw IllegalArgumentException("${typeInfo}")
        }
        return JobDetailBuilder(BaseUtil.defaultName(), BaseUtil.defaultGroup(), typeInfo)
    }

    public static func create<T>() where T <: IJob {
        return JobDetailBuilder(BaseUtil.defaultName(), BaseUtil.defaultGroup(), TypeInfo.of<T>())
    }

    public func withDescription(description: String) {
        _descripton = description
        return this
    }

    public func withIdentity(name: String) {
        _name = name
        return this
    }

    public func withIdentity(name: String, group: String) {
        _name = name
        _group = group
        return this
    }

    public func disableAllowConcurrentExecution() {
        _isAllowConcurrentExecution = false
        return this
    }

    public func build(): IJobDetail {
        return JobDetail(_name, _group, _typeInfo, _descripton, _isAllowConcurrentExecution)
    }
}
